libnet-bluetooth-perl (0.41-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Update lintian override info format in d/libnet-bluetooth-perl.lintian-overrides on line 2.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 13:51:28 +0000

libnet-bluetooth-perl (0.41-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 16:09:34 +0100

libnet-bluetooth-perl (0.41-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!

  [ intrigeri ]
  * Declare Rules-Requires-Root: no.
  * Enable all hardening build options.
  * Bump debhelper compatibility level to 11.
  * Declare compliance with Standards-Version 4.1.3.

 -- intrigeri <intrigeri@debian.org>  Sun, 21 Jan 2018 20:25:55 +0000

libnet-bluetooth-perl (0.41-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update path in lintian override.

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 0.41
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Fri, 09 Oct 2015 22:43:40 +0200

libnet-bluetooth-perl (0.40-3) unstable; urgency=low

  [ Jonathan Yu ]
  * Rewrite package long description (Closes: #597743)
  * Refresh patch and add DEP3 headers

  [ Rene Mayorga ]
  * Email change: Rene Mayorga -> rmayorga@debian.org

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * Replace proper-interpreter-in-samples.patch with an override in
    debian/rules.

  [ Xavier Guimard ]
  * Update copyright format to 1.0
  * Bump Standards-Version to 3.9.4
  * Use debhelper 9.20120312
  * Add lintian-overrides to avoid warning: hardening flags are present but
    not seen by lintian

 -- Xavier Guimard <x.guimard@free.fr>  Tue, 06 Nov 2012 21:41:08 +0100

libnet-bluetooth-perl (0.40-2) unstable; urgency=low

  [ gregor herrmann ]
  The 'ready for perl 5.10' release.

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
  * debian/rules: delete /usr/share/perl5 only if it exists.
  * Set Standards-Version to 3.7.3 (no changes needed).
  * Set debhelper compatibility level to 6.
  * debian/watch: use dist-based URL.
  * Remove debian/docs and install README directly from debian/rules.
  * debian/copyright:
    - mention version-independent upstream source location
    - extend reference to Perl license
  * debian/rules:
    - let build-stamp depend on $(DPATCH_STAMPFN) instead of patch-stamp
    - move dh_clean before make distclean
    - remove -stamp files with dh_clean
    - create install-stamp target depending on build-stamp
    - use DESTDIR and PREFIX for make install

  [ Damyan Ivanov ]
  * convert patches to quilt
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Wed, 23 Jan 2008 22:24:24 +0200

libnet-bluetooth-perl (0.40-1) unstable; urgency=low

  * Initial release (Closes: #441475)
  * '#!/usr/bin/perl' added to examples

 -- Rene Mayorga <rmayorga@debian.org.sv>  Sun, 09 Sep 2007 21:26:15 -0600
